#!/bin/bash
find $1 -name "$2" -exec echo {} \; -exec sh -c "cat {} | sed 's/\\n/\n/g' | sed 's/',/'\n/g' | grep failed= " \;

