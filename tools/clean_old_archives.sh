#!/bin/bash
NbSave=3

for (( c=1; c<=(${BUILD_NUMBER}-${NbSave}); c++ )) ; do
   rep_build="${WORKSPACE}/../builds/${c}"
   if [ -d "${rep_build}" ]; then
      echo "Clean build ${c}"
      rm -Rf "${rep_build}"
   fi
done
