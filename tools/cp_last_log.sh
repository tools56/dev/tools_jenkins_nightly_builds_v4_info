#!/bin/bash

login_ssh=$1
dirLogSSH=$2
dirLog=$3
nbJours=$4

nameKey="/var/jenkins_home/.ssh/id_jenkins"
#nameKey="/home/dockerAdmin/jenkins/.ssh/id_jenkins"

if [[ ${nbJours// } == "" ]]; then
   nbJours="-3"
fi

mkdir -p "${dirLog}"
rm -f "${dirLog}*"

lst_Files=$(ssh -i ${nameKey} ${login_ssh} "find ${dirLogSSH} -name '*.log' -mtime ${nbJours}" )
old_IFS=$IFS
IFS=$'\n'
for file_log in ${lst_Files} ; do
   file_log=${file_log// /\\ }
   scp -i ${nameKey} ${login_ssh}:"${file_log}" ${dirLog}
done
IFS=$old_IFS
ls -al "${dirLog}"
