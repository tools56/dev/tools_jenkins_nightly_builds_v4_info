#!/usr/bin/env groovy
def dateA = new Date().format('yyyy-MM-dd')
def repLogs="/Logs/"
def nameLogs= "report-*-" + dateA + ".log"
def localLogs="./logs/"

lst_files = ['./tools/psef.sh','./tools/view_crontab.sh']

key_jenkins="-i /var/jenkins_home/.ssh/id_jenkins"

def cmd_ssh(cmd) {
    sh """
       #!/bin/bash
       set -o pipefail
       ssh ${key_jenkins} ${name_ssh} "${cmd}" 2>&1 | tee /tmp/cmd_ssh.tmp
       RC=\${PIPESTATUS[0]}
       echo "RC=\${RC}"
       set +o pipefail
       exit \${RC}
    """.trim()
}

def get_login_ssh() {
    def login_ssh = "dockerAdmin" + '@' + "172.23.149.1"
    return login_ssh
}

def cp_file_ssh(file_cmd) {
    sh 'scp ' + key_jenkins + ' ' + file_cmd + ' ${name_ssh}:/tmp/' 
    String name_file = new File(file_cmd).getName();
    cmd="chmod +x /tmp/" + name_file
    cmd_ssh(cmd)
}
def cp_file_from_ssh(file_cmd, dir) {
    sh 'mkdir -p "./' + dir + '"'
    sh 'scp ' + key_jenkins + ' -q ' + ' ${name_ssh}:' + file_cmd + ' '+ dir
    sh 'ls -al  '+ dir
}

def rm_file_ssh(file_cmd) {
    String name = new File(file_cmd).getName();
    cmd_ssh("rm /tmp/"+name)
}

def cp_tools_ssh() {
    lst_files.each { item ->
        echo item
        cp_file_ssh(item)
    }
}
def rm_tools_ssh() {
    lst_files.each { item ->
        rm_file_ssh(item)
    }
}

pipeline {
    agent any
    options {
        disableConcurrentBuilds()
    }
    stages {
        stage('Init Param') {
            steps {
                script {
                    sh "printenv"
                    if (env.BUILD_USER.equals("admin") || env.BUILD_USER.equals("dev")) {
                       sh "echo Full Access"
                    }
                    if (params.Action.equals("")) {
                        currentBuild.result = 'ABORTED'
                        error('An action must be authorized. Aborting the build')
                    }
                    env.name_ssh=get_login_ssh()
               }
            }
        }
       stage('Copy Tools') {
            steps {
                  cp_tools_ssh()
            }
       }
       stage('Check docker') {
            steps {
                cmd_ssh("docker info")
                cmd_ssh('docker ps --format \'table {{.ID}} \t {{.Names}} \t {{.Status}}\' ')
            }
        }
        stage('Running') {
            steps {
                cmd_ssh("/tmp/psef.sh 'rebuildAndTestEnv'")
            }
        }
        stage('Copy Log In Local') {
            steps {
                sh "./tools/cp_last_log.sh ${name_ssh} ${repLogs} ${localLogs}"  
            }
        }
        stage('Log') {
            steps {
                sh "./tools/view_log.sh " + localLogs + " '" + nameLogs + "'" 
                sh "./tools/last_log.sh " + localLogs + " '" + nameLogs + "'"
            }
        }
        stage('Crontab') {
            steps {
                cmd_ssh("/tmp/view_crontab.sh")
            }
        }
        stage('Remove Tools') {
            steps {
                rm_tools_ssh()
            }
        }
        stage('Archive') {
           steps {
               archiveArtifacts artifacts: 'logs/*.log', allowEmptyArchive: 'true'
               sh "./tools/clean_old_archives.sh"
           }
       }
    }
}
